FROM ubuntu:22.04
MAINTAINER RWS Datalab <codebase.datalab@rws.nl>

# Set Debian front-end to non-interactive so that apt doesn't ask for
# prompts later.
ENV  DEBIAN_FRONTEND=noninteractive

RUN useradd runner --create-home && \
    # Create and change permissions for builds directory.
    mkdir -p /builds && \
    chown runner /builds && \
    export LC_ALL=C.UTF-8 && export LANG=C.UTF-8

# Use a new layer here so that these static changes are cached from above
# layer.  Update Ubuntu and install the build-deps.
RUN apt-get update -y -qq && \
    apt-get dist-upgrade -y -qq && \
    apt-get install -y -qq build-essential software-properties-common && \
    apt-get install -y -qq wget unzip git && \
    apt-get install -y -qq fonts-freefont-otf latexmk texlive-xetex xindy && \
    apt-get install -y -qq sudo && \
    apt-get install -y python3-venv python3-dev && \
    # Remove apt's lists to make the image smaller.
    rm -rf /var/lib/apt/lists/*

#  Add new user runner to sudo group
RUN adduser runner sudo

# Ensure sudo group users are not 
# asked for a password when using 
# sudo command by ammending sudoers file
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Switch to runner user and set the workdir.
USER runner
WORKDIR /home/runner

